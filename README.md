Illustrator Transform-Origin CSS Script
=======================================

A script for Adobe Illustrator to generate CSS with the `transform-origin` property set for each named object, useful in SVG animations.

## Installation

Clone or download this repository and put the folder or script file in the Illustrator `Presets/en_US/Scripts/` folder.
