﻿#target illustrator
#targetengine "main"

/**
 * AI Transform-Origin CSS Script
 *
 * @version 1.0.1
 * @author Cory Hughart <cory@coryhughart.com>
 */

var doc;
var items;
var w;
var cssArray;
var selectors;

function main() {
	if (app.documents.length == 0) {
		alert('Please open a document and rerun the script');
		return false;
	}

	cssArray = [];
	selectors = {};

	doc = app.activeDocument;
	items = doc.pageItems;
	// Allowing for a selection would break the duplicate id code
	/*
	items = doc.selection;
	if (items.length === 0) {
		items = doc.pageItems;
	}
	*/

	var p = Window.find('palette', 'Transform-Origin CSS');
	if (p == null) {
		p = new Window('palette', 'Transform-Origin CSS');
		p.align = 'left';
		p.alignChildren = 'fill';

		p.info = p.add('statictext', undefined, 'Processing object 0 of '+items.length+'...', {multiline: true});
		p.info.preferredSize = [350, -1];

		p.bar = p.add('progressbar', undefined, 0, items.length);
		p.bar.preferredSize = [350, -1];
	}

	p.show();

	for (var i = 0; i < items.length; i++) {
		p.info.text =  'Processing object '+i+' of '+items.length+'...';

		var pageItem = items[i];

		// Skip unnamed items
		if (pageItem.name != '') {
			var css = getCSS(pageItem);
			cssArray.push(css);
		}

		p.bar.value = i;
		// Update on every loop causes window to become unresponsive
		// Only update every 20 loops, adjust as needed
		if (i % 20 == 0) p.update();
	}
	p.update();
	p.close(); // will not close if palette became "unresponsive"...

	w = new Window('dialog', 'Transform-Origin CSS');
	w.copybox = w.add('edittext', undefined, cssArray.join(''), {multiline: true});
	w.copybox.preferredSize = [350, 400];

	var g = w.buttonGroup = w.add('group');
	g.alignment = 'right';

	w.alphabetize = g.add('button', undefined, 'Alphabetize');
	w.alphabetize.onClick = alphabetize;

	w.closeButton = g.add('button', undefined, 'Close', {name: 'cancel'});

	return w.show();
}

function getCenterPoint(pageItem) {
	var center = [0,0];
	center[0] = parseFloat((pageItem.position[0] + (pageItem.width / 2)).toFixed(4));
	center[1] = parseFloat(((0 - pageItem.position[1]) + (pageItem.height / 2)).toFixed(4));
	return center;
}

function getCssSelector(pageItem) {
	if (pageItem.name == '') return false;
	var name = pageItem.name;
	name = name.replace(/_/g, '_x5F_');
	name = name.replace(/\s/g, '_');
	if (selectors.hasOwnProperty(name)) {
		var dupname = name+'_'+selectors[name]+'_';
		selectors[name]++;
		name = dupname;
	}
	else {
		selectors[name] = 1;
	}
	var selector = '#'+name;
	return selector;
}

function getCSS(pageItem) {
	var centerPoint = getCenterPoint(pageItem);
	var css = '';
	css += getCssSelector(pageItem) + ' {\n';
	css += '  transform-origin: ' + centerPoint[0] + 'px ' + centerPoint[1] + 'px;\n';
	css += '}\n';
	return css;
}

function alphabetize() {
	cssArray.sort();
	var css = cssArray.join('');
	w.copybox.text = css;
}

main();
